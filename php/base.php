<?PHP

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Request Service</title>
		<link rel="stylesheet" type="css/text" href="../css/base.css">
		<link rel="stylesheet" type="css/text" href="../css/bootstrap.css">
		<script type="javascript/text" href="../js/jquery-1.11.3.js"></script>
		<script type="javascript/text" href="../js/base.js"></script>
		<script type="javascript/text" href="../js/bootstrao.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		<header class="container">
			<div class="row">
				<div class="col-xs-1 col-md-3"></div>
				<div id="dImgH" class="col-xs-10 col-md-6"><img src="../img/header.png" class="img-responsive"></div>
				<div id="dCloseS" class="col-xs-10 col-md-6"><button>Close Session</button></div>
				<div class="col-xs-1 col-md-3"></div>
			</div>
		</header>
		<div id="dBanner"></div>
		<nav>
			<ul id="uMenu">
				<li>Home</li>
				<li>Send</li>
				<li>Inbox</li>
				<li>Sent</li>
				<li>General Report</li>
			</ul>
		</nav>
		<main></main>
	</body>
</html>